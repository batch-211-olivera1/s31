// a. What directive is used by Node.js in loading the modules it needs?
	// answer: require()

// b. What Node.js module contains a method for server creation?
	// answer: HTTP module

// c. What is the method of the http object responsible for creating a server using Node.js?
	// answer: createServer()

// d. What method of the response object allows us to set status codes and content types?
	// answer: writeHead()

// e. Where will console.log() output its contents when run in Node.js?
	answer: Browsers

// f. What property of the request object contains the address' endpoint?
	// answer: listen()
