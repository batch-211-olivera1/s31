// Node JS Introduction


// used the "rquire" directive to load Node.js modules
// "module" is a software component or part of a program that contains one or more routines
// the "http module" lets node.js transfer data using the hypertext transfer protocol
// "http module" is a set of individual files that contain code to create a "component" that helps establish data transfer between applications
// http is a protocol that allow the fetching of resources such as html documents.
// clients (Browsers) and servers (node.js/express.js applications) communicate by exchanging individual messages
// the messages sent by the client, usually, a web browser, are called requests
// the messages sent by the server as an answer are called responses.


let http = require('http');

// http module has a createServer method that accepts a function as an argument and allows for a creation of a server
// the arguments passed in the function are request and response objects that contains method that allow us to receive requests from the client and send responses back to it
	
	http.createServer(function (request, response) {

	/*
		-use the "writeHead()" method to:
		-set a status code for the response- a 200 means OK
		-set the content-type of the response as a plain text message
	*/
	response.writeHead(200, {'Content-Type': 'text/plain'})
	// send the response with a text content "Hello World"
	response.end('Hello World');

}).listen(4000)
// port is a virtual point where network connections start and end
// each port is associated with a specific process or service
// server will be assigned to port 4000 via the "listen(4000)" method where the server will listen to any requests that are sent to it, eventually communicating with our server.

console.log('server running at localhost:4000');